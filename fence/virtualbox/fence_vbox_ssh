#!/usr/bin/python

# The Following Agent Has Been Tested On:
#
# VirtualBox 4.3.8 on Linux
#
# Based on VMware fencing agents by Eric Edgar at:
# https://github.com/eedgar/stonith-fences
# 

import sys
import pexpect
import exceptions
sys.path.append("/usr/share/fence")
from fencing import *


def get_outlets_status(conn, options):
    results = []
    vm_results = {}

    remove_chars = ['{', '}']

    try:
        conn.sendline("/usr/bin/vboxmanage -q list runningvms")
        conn.log_expect(options, options["-c"],int(options["-Y"]))
    except pexpect.EOF:
        fail(EC_CONNECTION_LOST)
    except pexpect.TIMEOUT:
        fail(EC_TIMED_OUT)

    results = conn.before.splitlines()[1:]

    for line in results:
        uuid = line.split()[1].translate(None, ''.join(remove_chars))
        vm_results[uuid] = "on"

    return vm_results


def get_power_status(conn, options):
    outlets=get_outlets_status(conn,options)
        
    return ((options["-n"] in outlets) and "on" or "off")


def set_power_status(conn, options):
    try:
        conn.sendline("/usr/bin/vboxmanage %s" % (options["-o"] == "on" and "startvm %s" % options['-n'] or "controlvm %s poweroff" % options['-n']))
        conn.log_expect(options, options["-c"], int(options["-g"]))
        time.sleep(1)
    except pexpect.EOF:
        fail(EC_CONNECTION_LOST)
    except pexpect.TIMEOUT:
        fail(EC_TIMED_OUT)


def main():
    device_opt = ["help", "version", "agent", "quiet", "verbose", "debug",
                  "action", "ipaddr", "login", "passwd", "passwd_script",
                  "secure", "identity_file", "ssh_options", "test", "port",
                  "separator", "inet4_only", "inet6_only", "ipport",
                  "power_timeout", "shell_timeout", "login_timeout", "power_wait"]

    atexit.register(atexit_handler)

    pinput = process_input(device_opt)
    pinput["-x"] = 1
    options = check_input(device_opt, pinput)
    ## Defaults for fence agent
    if not "-c" in options:
        options["-c"] = "\[EXPECT\]#\ "

    options["-X"] = "-t '/bin/bash -c \"PS1=\[EXPECT\]#\  /bin/bash --noprofile --norc\"'"

    docs = {}
    docs["shortdesc"] = "Fence agent for VirtualBox"
    docs["longdesc"] = "fence_vbox_ssh is a VirtualBox fencing agent \
for virtual machines. It uses ssh connection to the host system and the \
required vboxmanage commands."

    show_docs(options, docs)

    ## Operate the fencing device
    conn = fence_login(options)
    result = fence_action(conn, options, set_power_status, get_power_status, get_outlets_status)

    ## Logout from system
    try:
        conn.sendline("quit")
        conn.close()
    except exceptions.OSError:
        pass
    except pexpect.ExceptionPexpect:
        pass

    sys.exit(result)

if __name__ == "__main__":
    main()
